# Simple Feedback

## Debug in container

General errors can be found in stdout of the container.

```sh
docker exec -it test-wordpress /bin/bash
apt update && apt install nano
nano wp-config.php
```

Add just above the bottom:

```php
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
```

Exit then run

```sh
tail -f wp-content/debug.log
```
