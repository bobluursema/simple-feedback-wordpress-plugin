#!/bin/bash

docker compose \
  --file compose.yml \
  --project-name wp-plugin-test \
  up \
  --detach