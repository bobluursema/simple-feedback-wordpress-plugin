import { test as setup } from '@playwright/test'
import { execSync } from 'child_process'

setup('Delete WordPress', async () => {
  execSync('./delete-wp.sh', { cwd: process.cwd() + '/docker' })
})