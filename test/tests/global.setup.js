import { test as setup, expect } from '@playwright/test'
import { execSync } from 'child_process'
const path = require('path')

const WP_HOST = 'http://localhost'

setup('Perform WordPress install', async ({ page, request }) => {
  setup.setTimeout(100000)
  execSync('./delete-wp.sh', { cwd: process.cwd() + '/docker' })
  execSync('./start-wp.sh', { cwd: process.cwd() + '/docker' })

  const startTime = Date.now()
  while (true) {
    try {
      await page.goto(WP_HOST)
      await new Promise(r => setTimeout(r, 3000))
      await page.reload()
      console.log(await page.title())
      if ((await page.title()) === 'WordPress › Installation') {
        break
      }
    } catch (err) {
      await new Promise(r => setTimeout(r, 3000))
    }
    if ((Date.now() - startTime) > 60000) {
      throw Error("Wordpress or DB didn't load within one minute")
    }
  }

  await expect(page).toHaveTitle('WordPress › Installation')
  // This continue button is on the language selector screen, but that page doesn't always appear?
  await page.getByText('Continue').click()
  await page.getByLabel('Site Title').fill('Plugin Test')
  await page.getByLabel('Username').fill('admin')
  await page.getByLabel('Password', { exact: true }).fill('abcABC123!@#')
  await page.getByLabel('Your Email').fill('test@projectsunset.nl')
  await page.getByText('Install WordPress').click()
  await expect(page.getByText('Success!')).toBeVisible()

  // Install plugin
  await page.goto(`${WP_HOST}/wp-admin/`)
  await expect(page).toHaveTitle('Log In ‹ Plugin Test — WordPress')
  await page.getByLabel('Username or Email Address').fill('admin')
  await page.getByLabel('Password', { exact: true }).fill('abcABC123!@#')
  await page.getByText('Log In').click()
  await expect(page.getByRole('link', { name: 'Dashboard' })).toBeVisible()
  if (await page.getByRole('link', { name: 'Plugins', exact: true }).isVisible()) {
    await page.getByRole('link', { name: 'Plugins', exact: true }).click()
  } else {
    await page.getByRole('link', { name: 'Plugins 1', exact: true }).click()
  }
  await page.locator('#wpbody-content').getByRole('link', { name: 'Add New Plugin' }).click()
  await page.getByText('Upload Plugin', { exact: true }).click()
  await page.getByLabel('Plugin zip file').setInputFiles(path.join(__dirname, '..', '..', 'simple-feedback.zip'))
  await page.getByRole('button', { name: 'Install Now' }).click()
  await expect(page.getByText('Plugin installed successfully.')).toBeVisible()
  await page.getByText('Activate Plugin', { exact: true }).click()
  await expect(page.getByText('Plugin activated.', { exact: true })).toBeVisible()
})