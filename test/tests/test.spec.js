// @ts-check
const { test, expect } = require('@playwright/test')

const WP_HOST = 'http://localhost'

test('Basic functionality', async ({ page, browser }) => {
  test.setTimeout(120000)
  // Check plugin is currently disabled
  const context = await browser.newContext()
  const visitorPage = await context.newPage()
  await visitorPage.goto(WP_HOST)
  await visitorPage.waitForTimeout(6000)
  await expect(visitorPage.getByLabel('Heb je gevonden wat je zocht?')).toBeHidden()

  // Enable plugin
  await page.goto(`${WP_HOST}/wp-admin/`)
  await expect(page).toHaveTitle('Log In ‹ Plugin Test — WordPress')
  await page.getByLabel('Username or Email Address').fill('admin')
  await page.getByLabel('Password', { exact: true }).fill('abcABC123!@#')
  await page.getByText('Log In').click()
  await page.getByRole('link', { name: 'Simple Feedback', exact: true }).click()
  await page.getByRole('checkbox').check()
  await page.getByRole('button', { name: 'Save Changes' }).click()

  // Check that popup now appears
  await visitorPage.reload()
  await page.waitForTimeout(6000)
  await expect(visitorPage.getByLabel('Heb je gevonden wat je zocht?')).toBeVisible({ timeout: 4000 })

  // Fill in popup and submit
  await visitorPage.getByLabel('Heb je gevonden wat je zocht?').fill('Zeker weten!')
  await visitorPage.getByRole('button', { name: 'Verzenden' }).click()
  await expect(visitorPage.getByText('Bedankt!')).toBeVisible()

  // Check message has arrived in admin
  await page.reload()
  await expect(page.getByText('Zeker weten!')).toBeVisible()
  await page.getByRole('button', { name: 'Verwijder' }).click()
  await expect(page.getByText('Zeker weten!')).toBeHidden()

  // Check that popup doesn't reappear
  await visitorPage.reload()
  await page.waitForTimeout(6000)
  await expect(visitorPage.getByLabel('Heb je gevonden wat je zocht?')).toBeHidden()
})

