=== Simple Feedback ===
Contributors: bob-luursema
Tags: feedback
Requires at least: 4.5.0
Tested up to: 6.4.1
Requires PHP: 8.0
Stable tag: 1.0
License: GPL v2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The most simple plugin to ask feedback from your visitors.

== Description ==
This plugin adds a small sliding popup on your website where users can fill in feedback for you. Once a visitor has given feedback the popup won\'t appear again for them (or to be more precise, no longer in that browser until they clear localStorage).

All responses are stored in the database and you can view and delete them in an admin page. On that page you can also enable and disable the popup.