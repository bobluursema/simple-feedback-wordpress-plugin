<?php

namespace SimpleFeedback\Setup;

use SimpleFeedback\Admin\Hooks as HooksAdmin;
use SimpleFeedback\Web\Hooks as HooksWeb;

/**
 * Calls the admin and public hooks classes that setup everything.
 */
class Plugin {
	private $plugin_name;
	private $table_name;
	public function __construct() {
		$this->plugin_name = "SimpleFeedback";
		global $wpdb;
		$this->table_name = $wpdb->prefix . Installer::$table_name;
	}

	/**
	 * Is called by the main file on execution.
	 */
	public function setup_hooks() {
		$this->setup_admin_hooks();
		$this->setup_public_hooks();
	}

	private function setup_admin_hooks() {
		$plugin_admin = new HooksAdmin( $this->plugin_name, $this->table_name );
		$plugin_admin->setup_hooks();
	}

	private function setup_public_hooks() {
		$plugin_public = new HooksWeb( $this->plugin_name, $this->table_name );
		$plugin_public->setup_hooks();
	}
}