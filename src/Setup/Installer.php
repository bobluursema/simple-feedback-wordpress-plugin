<?php

namespace SimpleFeedback\Setup;

/**
 * Handles activation and uninstalling.
 */
class Installer {

	public static $table_name = "sf_responses";

	// All public ones are automatically added to the Web API.
	public static $setting_names = array(
		"enabled" => array(
			"name" => "simple-feedback-enabled",
			"type" => "boolean",
			"public" => true
		)
	);

	/**
	 * Creates the database table to store the responses.
	 */
	public static function activate() {
		global $wpdb;
		$table_name = $wpdb->prefix . Installer::$table_name;
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    time datetime DEFAULT NOW() NOT NULL,
    url varchar(255) NOT NULL,
    text text NOT NULL,
    PRIMARY KEY  (id)
  ) $charset_collate;";

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );
	}

	/**
	 * Deletes the database table and all options.
	 */
	public static function uninstall() {
		global $wpdb;
		$table_name = $wpdb->prefix . Installer::$table_name;
		$wpdb->query( "DROP TABLE IF EXISTS `$table_name`" );

		foreach (Installer::$setting_names as $name) {
			delete_option( $name );
		}
	}
}