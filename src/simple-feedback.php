<?php
/*
 * Plugin Name: Simple Feedback
 * Plugin URI: https://www.projectsunset.nl/wordpress-simple-feedback-plugin/
 * Description: Simple feedback collection with a popup
 * Version: 1.0
 * Author: Bob Luursema
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

namespace SimpleFeedback;

// Import all the files, one could use Composer for autoloading or write
// a complicated autoload function. But this is just six files.
require_once plugin_dir_path( __FILE__ ) . 'Setup/Installer.php';
require_once plugin_dir_path( __FILE__ ) . 'Setup/Plugin.php';
require_once plugin_dir_path( __FILE__ ) . 'Admin/Hooks.php';
require_once plugin_dir_path( __FILE__ ) . 'Admin/Controller.php';
require_once plugin_dir_path( __FILE__ ) . 'Web/Hooks.php';
require_once plugin_dir_path( __FILE__ ) . 'Web/Controller.php';

// Register the activation and uninstall hooks
register_activation_hook( __FILE__, [ __NAMESPACE__ . '\\Setup\\Installer', 'activate' ] );
register_uninstall_hook( __FILE__, [ __NAMESPACE__ . '\\Setup\\Installer', 'uninstall' ] );

// Run the plugin!
function run_simple_feedback() {
	$plugin = new Setup\Plugin();
	$plugin->setup_hooks();
}
run_simple_feedback();
