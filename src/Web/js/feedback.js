window.addEventListener('load', (event) => {

  const storageName = 'simplefeedback'

  function retrieveStorage() {
    const value = localStorage.getItem(storageName)
    if (value) {
      return value
    }
    localStorage.setItem(storageName, 'no_action')
    return 'no_action'
  }

  function showForm() {
    const form = document.createElement('form')
    form.id = 'feedback-form'
    form.onsubmit = submit
    form.innerHTML = `
      <div>
        <h2>Feedback</h2>
        <button id="close-feedback" class="feedback-form-arrow-down" type="button"></button>
      </div>
      <label for="feedback">Heb je gevonden wat je zocht?</label>
      <textarea rows="3" id="feedback" name="feedback"></textarea>
      <button type="submit">Verzenden</button>
    `
    form.style.visibility = 'hidden'
    document.body.appendChild(form)
    form.style.bottom = `-${form.offsetHeight + 50}px`
    const closeButton = document.getElementById('close-feedback')
    closeButton.onclick = close
    requestAnimationFrame(() => {
      setTimeout(() => {
        form.style.visibility = 'visible'
        form.style.bottom = '0'
      }, 1000)
    })
  }

  function close(event) {
    const form = document.getElementById('feedback-form')
    if (event.target.classList.contains("feedback-form-arrow-down")) {
      document.getElementById('feedback-form').style.bottom = `-${form.offsetHeight - 40}px`
    } else {
      document.getElementById('feedback-form').style.bottom = '0'
    }
    event.target.classList.toggle("feedback-form-arrow-down")
    event.target.classList.toggle("feedback-form-arrow-up")
  }

  function submit(event) {
    event.preventDefault()
    const form = document.getElementById('feedback-form')
    form.querySelector('button').disabled = true
    const formData = new FormData(form)
    formData.append('location', document.location.pathname)
    fetch('/wp-json/simple-feedback/v1/submit', {
      method: 'post',
      body: formData
    }).finally(() => {
      localStorage.setItem(storageName, `${Date.now()}`)
      const computedStyle = getComputedStyle(form)
      const originalWidth = form.clientWidth - parseFloat(computedStyle.paddingLeft) - parseFloat(computedStyle.paddingRight)
      const thanksHeight = form.clientHeight - parseFloat(computedStyle.paddingTop) - parseFloat(computedStyle.paddingBottom) - form.querySelector('div').offsetHeight
      form.innerHTML = `
      <div>
        <h2>Feedback</h2>
        <button id="close-feedback" class="feedback-form-arrow-down" type="button"></button>
      </div>
      <p style="height: ${thanksHeight}px; margin: 0">Bedankt!</p>
      `
      form.style.width = originalWidth + 'px'
      setTimeout(() => {
        form.style.bottom = `-${form.offsetHeight + 30}px`
      }, 2000)
    })
  }

  const currentValue = retrieveStorage()

  if (currentValue === 'no_action') {
    fetch('/wp-json/simple-feedback/v1/settings').then(r => r.json()).then(settings => {
      if (settings.enabled) {
        setTimeout(showForm, 4000)
      }
    })
  }
})
