<?php

namespace SimpleFeedback\Web;

use SimpleFeedback\Setup\Installer as Installer;

/**
 * REST API endpoints for the public side.
 */
class Controller {
	private $namespace;
	private $table_name;
	public function __construct( $table_name ) {
		$this->namespace = 'simple-feedback/v1';
		$this->table_name = $table_name;
	}

	/**
	 * Creates the REST API routes.
	 * 
	 * Called by the Hooks class during `rest_api_init`.
	 */
	public function register_routes() {
		register_rest_route( $this->namespace, '/submit', array(
			'methods' => 'POST',
			'callback' => array( $this, 'submit' ),
			'permission_callback' => '__return_true'));
		
		register_rest_route( $this->namespace, '/settings', array(
			'methods' => 'GET',
			'callback' => array( $this, 'get_settings' ),
			'permission_callback' => '__return_true'));
	}

	/**
	 * Handles the call from a user to submit feedback.
	 */
	public function submit( $request ) {
		global $wpdb;
		$wpdb->insert(
			$this->table_name,
			array(
				'text' => $request['feedback'],
				'url' => $request['location'],
			)
		);
		return new \WP_REST_Response(null, 204);
	}

	/**
	 * Handles the call to get the current settings.
	 *
	 * This is handled via an API call so that the cache does not
	 * need to be cleared when you change any of the settings.
	 */
	public function get_settings() {
		$public_settings = array();
		foreach (Installer::$setting_names as $key => $config) {
			if (!$config["public"]) {
				continue;
			}
			if ($config["type"] == "boolean") {
				$public_settings[$key] = get_option($config["name"]) == 'on';
			} else {
				$public_settings[$key] = get_option($config["name"]);
			}
		}
		return $public_settings;
	}
}