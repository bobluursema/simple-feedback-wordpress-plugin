<?php

namespace SimpleFeedback\Web;

/**
 * Setups all the hooks into Wordpress for the public side.
 * 
 * Called by Setup\Plugin during execution.
 */
class Hooks {
	private $plugin_name;
	private $table_name;
	public function __construct( $plugin_name, $table_name ) {
		$this->plugin_name = $plugin_name;
		$this->table_name = $table_name;
	}

	/**
	 * Setups all hooks into Wordpress for the public side.
	 */
	public function setup_hooks() {
		// Add JS and CSS to every page
		add_action( 'wp_enqueue_scripts', function () {
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/feedback.js' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/feedback.css' );
		} );

		// Create the API routes
		add_action( 'rest_api_init', function () {
			$controller = new Controller( $this->table_name );
			$controller->register_routes();
		} );
	}
}