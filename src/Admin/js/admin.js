document.addEventListener('DOMContentLoaded', (event) => {

  /**
   * Handle delete button click
   * 
   * Calls the REST endpoint to delete the row in the database and removes the row in the UI
   * @param {PointerEvent} event 
   */
  function submit(event) {
    event.preventDefault()
    event.target.disabled = true
    const formData = new FormData()
    formData.append('id', event.target.dataset.id)
    fetch('/wp-json/simple-feedback/v1/delete', {
      method: 'post',
      body: formData,
      headers: {
        'X-WP-Nonce': _wp_nonce
      }
    }).then((response) => {
      if (response.ok) {
        const tr = event.target.closest('tr')
        tr.remove()
      } else {
        console.error("Deleting the response failed.")
      }
    })
  }

  // Bind function to the delete button of every response
  document.querySelectorAll('#feedback-responses button').forEach(button => {
    button.onclick = submit
  })

})
