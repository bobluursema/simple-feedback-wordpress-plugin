<?php

namespace SimpleFeedback\Admin;

use SimpleFeedback\Setup\Installer as Installer;

/**
 * Setups all the hooks into Wordpress for the Admin side.
 *
 * Called by Setup\Plugin during execution.
 */
class Hooks {
	private $plugin_name;
	private $table_name;
	public function __construct( $plugin_name, $table_name ) {
		$this->plugin_name = $plugin_name;
		$this->table_name = $table_name;
	}

	/**
	 * Setups all hooks into Wordpress for the admin side.
	 */
	public function setup_hooks() {

		// Capability used in the REST API
		add_action( 'init', function () {
			$role = get_role( 'administrator' );
			$role->add_cap('delete_simple_feedback', true);
		}, 11);

		// Create aforementioned admin REST API
		add_action( 'rest_api_init', function () {
			$controller = new Controller( $this->table_name );
			$controller->register_routes();
		} );

		// Scripts and styles for the responses viewer and deletion of them
		add_action( 'admin_enqueue_scripts', function ( $hook_suffix  ) {
			if ($hook_suffix != "toplevel_page_simple-feedback"){
				return;
			}
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/admin.css' );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/admin.js' );
			wp_add_inline_script( $this->plugin_name, 'const _wp_nonce = "' . wp_create_nonce( 'wp_rest' ) . '"');
		} );

		// Create top level admin page
		add_action( 'admin_menu', function () {
			add_menu_page(
				'Simple Feedback',
				'Simple Feedback',
				'manage_options',
				'simple-feedback',
				array( $this, 'render_admin_page' ) );
		} );

		// Setup settings for the plugin, these will be rendered on the admin page
		add_action( 'admin_init', function() {
			$enabled_name = Installer::$setting_names["enabled"]["name"];
			register_setting('simple-feedback', $enabled_name);
			
			add_settings_section(
				'simple-feedback-general',
				'General settings',
				function() {
					echo "";
				},
				'simple-feedback');
			
			add_settings_field(
				$enabled_name,
				'Feedback form Active',
				function() {
					$enabled_name = Installer::$setting_names["enabled"]["name"];
					$current_value = get_option($enabled_name);
					echo '<input type="checkbox" name="' . $enabled_name . '" ' . checked($current_value, 'on', false) . ' />';
				},
				'simple-feedback',
				'simple-feedback-general');
		});
	}

	/**
	 * Renders the admin UI.
	 */
	public function render_admin_page() {
		include_once 'ui.php';
	}
}