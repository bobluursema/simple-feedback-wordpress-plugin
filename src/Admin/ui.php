<h1>Simple Feedback</h1>
<form action='options.php' method='post'>
	<?php
		settings_fields('simple-feedback');
		do_settings_sections('simple-feedback');
		submit_button();
		?>
</form>
<h2>Responses</h2>
<?php
global $wpdb;
$responses = $wpdb->get_results(
	"
		SELECT ID, time, url, text
		FROM {$wpdb->prefix}sf_responses
	" );
if (count($responses) == 0){
	?>
	<p>Nog geen responses ontvangen</p>
	<?php
} else {
?>
<table id="feedback-responses">
	<thead>
		<tr>
			<th>Tijdstip</th>
			<th>Pagina</th>
			<th>Bericht</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ( $responses as $response ) {
				?>
				<tr>
					<td>
						<?= $response->time ?>
					</td>
					<td>
						<?= $response->url ?>
					</td>
					<td>
						<?= $response->text ?>
					</td>
					<td>
						<button type="button" data-id="<?= $response->ID ?>">Verwijder</button>
					</td>
				</tr>
				<?php
			}
}
		?>
	</tbody>
</table>