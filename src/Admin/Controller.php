<?php

namespace SimpleFeedback\Admin;

/**
 * REST API endpoints for the admin side.
 */
class Controller {
	private $namespace;
	private $table_name;
	public function __construct( $table_name ) {
		$this->namespace = 'simple-feedback/v1';
		$this->table_name = $table_name;
	}

	/**
	 * Creates the REST API routes.
	 *
	 * Called by the Hooks class during `rest_api_init`.
	 */
	public function register_routes() {
		register_rest_route( $this->namespace, '/delete', array(
			// When using DELETE verb the id sent in the body isn't present
			'methods' => 'POST',
			'callback' => array( $this, 'delete' ),
      'permission_callback' => function () {
        return current_user_can( 'delete_simple_feedback' );
      }
    ) );
	}

	/**
	 * Handles the call from the admin UI to delete a response.
	 */
	public function delete( $request ) {
		global $wpdb;
		$wpdb->delete(
			$this->table_name,
			array(
				'ID' => $request['id'],
			),
			array(
				'%d'
			)
		);
		return new \WP_REST_Response(null, 204);
	}
}